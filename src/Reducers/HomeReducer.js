
import * as ActionType from "../Actions/ActionType";

// const initialState = {
//   data: undefined,
//   images: undefined,
//   buttonData: undefined
// };

import jsonfile from "./data.json";

export default (state = jsonfile, action) => {
  switch (action.type) {
    case ActionType.GET_DATA:
      return {
        ...state,
        data: action.data,
      };
    case ActionType.GET_IMAGES:
      return {
        ...state,
        images: action.data,
      };
    case ActionType.BUTTON_DATA:
      return {
        ...state,
        buttonData: action.button,
    };

    default:
      return state;
  }
};

