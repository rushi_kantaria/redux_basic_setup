import { combineReducers } from "redux";

// import HomeReducer from "./HomeReducer";
import CounterReducer from "./CounterReducer";

const rootReducer = combineReducers({
    // homeReducer: HomeReducer,
    counterReducer: CounterReducer
});

export default rootReducer;