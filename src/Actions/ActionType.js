export const GET_DATA                           =  "GET_DATA";
export const GET_IMAGES                         = "GET_IMAGES";
export const BUTTON_DATA                        = "BUTTON_DATA";

//counter function
export const INCREMENT                           = "INCREMENT";
export const DECREMENT                           = "DECREMENT";