import * as ActionType from "./ActionType";

export const fetchData  = () => {
    return async function(dispatch) {
        let data = await fetch(
            "http://143.110.254.46/emerso-backend/api/get-planet"
        )
        .then(response => response.json())
        .then(data => data);
        return dispatch({ type: "GET_DATA", data });
    }
}

export const fetchImage  = () => {
    return async function(dispatch) {
        let data = await fetch(
            "http://143.110.254.46/emerso-backend/api/configuration"
        )
        .then(response => response.json())
        .then(data => data);
        return dispatch({ type: ActionType.GET_IMAGES, data });
    }
}