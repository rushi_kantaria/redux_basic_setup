import React from "react";
import { createStore, combineReducers } from "redux";
import { Provider } from "react-redux";
import { render, fireEvent, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";
import CounterScreen from "./CounterScreen";
import CounterReducer from "../Reducers/CounterReducer";

afterEach(cleanup)

const rootReducer = combineReducers({
    counterReducer: CounterReducer
});

function renderWithRedux(
    component,
    {initialState, store = createStore(rootReducer, initialState)} = {}
){
    return {
        ...render(<Provider store={store}>{component}</Provider>)
    }
}

test("render with redux", () => {
    const { getByTestId } = renderWithRedux(<CounterScreen />);
    expect(getByTestId('count')).toHaveTextContent("0")
})

test("can increment", () => {
    const {getByTestId, getByText} = renderWithRedux(<CounterScreen />);
    fireEvent.click(getByText("Increment"))
    expect(getByTestId('count')).toHaveTextContent("1")
})

test("can decrement", () => {
    const {getByTestId, getByText} = renderWithRedux(<CounterScreen />);
    fireEvent.click(getByText("Decrement"))
    expect(getByTestId('count')).toHaveTextContent("-1")
})

test("can have intial state", () => {
    const { getByTestId } = renderWithRedux(<CounterScreen />, {
        initialState: { counterReducer: {count: 5 }}
    });
    expect(getByTestId('count')).toHaveTextContent("5")
})

test("can have custome store", () => {
    const store = createStore(() => ({counterReducer: {count: 35 }}))
    const { getByTestId } = renderWithRedux(<CounterScreen />, { store });
    expect(getByTestId('count')).toHaveTextContent("35")
})