import React from 'react';
import { connect } from "react-redux";
import { IncrementCount, DecrementCount } from "../Actions/CounterAction";

const CounterScreen = (props) => {

    const Increment = () => {
        props?.IncreaseCount()
    }

    const Decrement = () => {
        props?.DecrementCounter()
    }

    return (
      <div>
            <h1 onClick={() => Increment()} style={{cursor: "pointer"}}>Increment</h1>
            <h1 data-testid="count">{props.count}</h1>
            <h1 onClick={() => Decrement()} style={{cursor: "pointer"}}>Decrement</h1>
      </div>
    );
  }

  const mapStateToProps = (state) => ({
    count: state.counterReducer.count,
  });
  
  const mapDispatchToProps = dispatch => {
    return {
      IncreaseCount: () => {dispatch(IncrementCount())},
      DecrementCounter: () => {dispatch(DecrementCount())}
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(CounterScreen);