import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import { fetchData } from "../Actions/HomeAction";
import { bindActionCreators } from "redux";

const TestingScreen = (props) => {
    
    return (
      <div>
        <h1 style={{color: "red", cursor: "pointer" }}>{props.buttonData}</h1>
      </div>
    );
  }

  const mapStateToProps = (state) => ({
    buttonData: state.homeReducer.buttonData,
  });
  
  export default connect(
    mapStateToProps,
    null
  )(TestingScreen);