import React from 'react';
import { connect } from "react-redux";

const HomeScreen = (props) => {

    console.log("button data ----->", props.buttonData);
    const handelClick = async(button) => {
        await props.addItem(button);
        props.history.push("TestingScreen");
    }

    return (
      <div>
        <h1 onClick={() => handelClick("button1")} style={{color: props.buttonData === "button1" && "red", cursor: "pointer" }}>button 1</h1>
        <h1 onClick={() => handelClick("button2")} style={{color: props.buttonData === "button2" && "red", cursor: "pointer" }}>button 2</h1>
        <h1 onClick={() => handelClick("button3")} style={{color: props.buttonData === "button3" && "red", cursor: "pointer" }}>button 3</h1>
        <h1 onClick={() => handelClick("button4")} style={{color: props.buttonData === "button4" && "red", cursor: "pointer" }}>button 4</h1>
      </div>
    );
  }

  const mapStateToProps = (state) => ({
    data: state.homeReducer.data,
    buttonData: state.homeReducer.buttonData,
  });
  
  const mapDispatchToProps = dispatch => {
    return {
      addItem: (button) => { dispatch({ type: "BUTTON_DATA", button })}
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(HomeScreen);