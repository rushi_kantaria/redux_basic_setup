import './App.css';
import { Provider } from "react-redux";
import store from "./store";
// import HomeScreen from "./Screens/HomeScreen";
import TestingScreen from "./Screens/TestingScreen";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import CounterScreen from './Screens/CounterScreen';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
      <BrowserRouter>
            <Switch>
              <Route path="/" exact component={CounterScreen} />
              <Route path="/TestingScreen" exact component={TestingScreen} />
            </Switch>
          </BrowserRouter>
      </div>
    </Provider>
  );
}

export default App;
